const fs = require("fs");
const XLSX = require("xlsx");
const https = require("https");
const xml2js = require("xml2js");

// This function converts the excel sheet to a json object and returns the object
function sheet_to_json(path) {
  const workbook = XLSX.readFile(path);
  let worksheets = {};
  for (const sheetName of workbook.SheetNames) {
    worksheets[sheetName] = XLSX.utils.sheet_to_json(
      workbook.Sheets[sheetName]
    );
  }
  // to access different sheets just add the sheet number. i.e Sheet2 for second sheet
  let data = worksheets.Sheet2;
  return data;
}

// This function returns the xml data from the xml page
function get_xml_page(url) {
  return new Promise(function (resolve, reject) {
    https
      .get(url, (response) => {
        let data = "";

        // If the page doesnt loads up properly, if the url is wrong or cannot find the page
        if (response.statusCode < 200 || response.statusCode > 299) {
          return reject("err");
        }
        response.on("data", (chunk) => {
          data += chunk;
        });

        response.on("end", () => {
          return resolve(data);
        });
      })
      // If error when getting the data from from xml page
      .on("error", (err) => {
        return reject("err");
      });
  });
}

// To check if the file is a htm file
function checkifHTM(str) {
  const myArray = str.split(".");
  if (myArray[1] == "htm") {
    return true;
  } else {
    return false;
  }
}

// Downloads the htm file and stores it in the htmfiles folder
function get_htm_page(fileurl, filename) {
  return new Promise(function (resolve, reject) {
    https.get(fileurl, (res) => {
      const path = `./htmfiles/${filename}`;
      const writeStream = fs.createWriteStream(path);
      if (res.statusCode < 200 || res.statusCode > 299) {
        return reject("err");
      }

      res.pipe(writeStream);

      writeStream
        .on("finish", () => {
          writeStream.close();
          return resolve("Download Completed!");
        })
        .on("error", (err) => {
          return reject("err");
        });
    });
  });
}

async function main() {
  // const all_data_json = sheet_to_json("./reference-data-sheet2.xlsx");
  const all_data_json = sheet_to_json("./reference-data-sheet2.xlsx");
  let url_that_dosent_work = [];
  let filename;

  // loop through the excel sheet
  for (let i = 0; i < all_data_json.length; i++) {
    let url = `https://resources-testing.kriyadocs.com/resources/integration/${all_data_json[i].Customer}/${all_data_json[i].Project}/${all_data_json[i].Article_id}/resources/file_list.xml`;

    let xml_page_data;

    // gets the data from the url and stores it in the xml_page_data variable
    try {
      xml_page_data = await get_xml_page(url);
    } catch (err) {
      xml_page_data = "error in getting xml page";
    }

    // In case of errors, we store them in a separate variable url_that_dosent_work
    if (xml_page_data == "error in getting xml page") {
      url_that_dosent_work.push(url);
      continue;
    } else {
      let json_data;
      xml2js.parseString(xml_page_data, (err, result) => {
        if (err) {
          throw err;
        }
        json_data = JSON.stringify(result, null, 4);
      });
      json_data = JSON.parse(json_data);

      // loops through the file tag in the xml
      for (let j = 0; j < json_data.files.file.length; j++) {
        if (
          json_data.files.file[j].$.file_designation == "manuscript" &&
          json_data.files.file[j].$.data_remove != "true"
        ) {
          // searches for a .htm file and stores it in a variable filename
          if (checkifHTM(json_data.files.file[j].$.file_name)) {
            filename = json_data.files.file[j].$.file_name;
          } else if (checkifHTM(json_data.files.file[j].$.file_alt_name)) {
            filename = json_data.files.file[j].$.file_alt_name;
          } else {
            filename = false;
          }
          console.log(filename);
          let fileurl = `https://resources-testing.kriyadocs.com/resources/integration/${all_data_json[i].Customer}/${all_data_json[i].Project}/${all_data_json[i].Article_id}/resources/${filename}`;
          let finalstep;

          // Downloads the htm file
          try {
            finalstep = await get_htm_page(fileurl, filename);
          } catch (err) {
            finalstep = "Error in downloading .htm file";
          }
          console.log(finalstep, i);
          console.log(fileurl);
        }
      }
    }
  }
  console.log("urls that dosent work: \n", url_that_dosent_work);
}
main();
